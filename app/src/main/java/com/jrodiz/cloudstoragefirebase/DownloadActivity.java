package com.jrodiz.cloudstoragefirebase;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.annimon.stream.Stream;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.jrodiz.cloudstoragefirebase.adapter.ImageAdapter;
import com.jrodiz.cloudstoragefirebase.ctrl.JImageController;
import com.jrodiz.cloudstoragefirebase.obj.JImage;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

import static com.annimon.stream.Collectors.toList;

public class DownloadActivity extends AppCompatActivity implements ImageAdapter.ImageAdapterInterface, JImageController.JImageInterface {

    private final List<JImage> mImages = new ArrayList<>();
    private RecyclerView.Adapter mAdapter;
    private final JImageController mJImageController = new JImageController(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mAdapter = new ImageAdapter(this);
        recyclerView.setAdapter(mAdapter);

        mJImageController.syncDownloadStorage();

    }

    @Override
    public void onImagesUpdated(List<JImage> jImages) {
        mImages.clear();
        mImages.addAll(Stream.of(jImages).filter(JImage::isIncoming).collect(toList()));
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public List<JImage> getData() {
        return mImages;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sync:
                mJImageController.syncDownloadStorage();
                break;
        }
        return true;
    }

    @Override
    public boolean isAlive() {
        return !isFinishing();
    }

    @Override
    public Context getViewContext() {
        return this;
    }
}
