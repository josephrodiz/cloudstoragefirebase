package com.jrodiz.cloudstoragefirebase.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.daimajia.numberprogressbar.NumberProgressBar;
import com.jrodiz.cloudstoragefirebase.MLImageActivity;
import com.jrodiz.cloudstoragefirebase.R;
import com.jrodiz.cloudstoragefirebase.base.AppConstants;
import com.jrodiz.cloudstoragefirebase.base.IRptContext;
import com.jrodiz.cloudstoragefirebase.obj.JImage;
import com.jrodiz.cloudstoragefirebase.utils.ImageUtility;

import java.lang.ref.WeakReference;
import java.util.List;

import jp.wasabeef.glide.transformations.CropCircleTransformation;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;


public class ImageAdapter extends RecyclerView.Adapter {

    public interface ImageAdapterInterface extends IRptContext<Context> {
        List<JImage> getData();
    }

    private final ImageAdapterInterface mListener;

    public ImageAdapter(ImageAdapterInterface listener) {
        this.mListener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(mListener.getViewContext()).inflate(R.layout.image_row, viewGroup, false);
        ImageViewHolder vh = new ImageViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder vh, int i) {
        JImage image = mListener.getData().get(i);

        ImageViewHolder ivh = (ImageViewHolder) vh;

        ivh.itemView.setOnClickListener(new ImageClickListener(mListener.getViewContext(), image));
        ivh.mNameView.setText(image.getName());
        ivh.mProgressBar.setProgress((int) image.getProgress());

        Glide.with(mListener.getViewContext())
                .load(image.getPath())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.placeholder_image))
                .apply(RequestOptions.bitmapTransform(new RoundedCornersTransformation(
                        (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6,
                                mListener.getViewContext().getResources().getDisplayMetrics()),
                        20)))
                .into(ivh.mImageView);
    }

    @Override
    public int getItemCount() {
        return mListener.getData().size();
    }

    private static class ImageClickListener implements View.OnClickListener {
        private final WeakReference<Context> mContext;
        private final JImage mJImage;

        private ImageClickListener(Context c, JImage image) {
            this.mContext = new WeakReference<>(c);
            this.mJImage = image;
        }

        @Override
        public void onClick(View v) {
            Intent imageMl = new Intent(mContext.get(), MLImageActivity.class);
            Bundle p = new Bundle();
            p.putParcelable(AppConstants.IMAGE_KEY, mJImage);
            imageMl.putExtras(p);
            mContext.get().startActivity(imageMl);
        }
    }

    public static class ImageViewHolder extends RecyclerView.ViewHolder {
        private ImageView mImageView;
        private TextView mNameView;
        private NumberProgressBar mProgressBar;


        public ImageViewHolder(View v) {
            super(v);
            mImageView = v.findViewById(R.id.image);
            mNameView = v.findViewById(R.id.name);
            mProgressBar = v.findViewById(R.id.progres);
        }
    }
}
