package com.jrodiz.cloudstoragefirebase.firebase.ml;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetector;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetectorOptions;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.common.FirebaseVisionPoint;
import com.google.firebase.ml.vision.face.FirebaseVisionFace;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetector;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceLandmark;
import com.google.firebase.ml.vision.label.FirebaseVisionLabel;
import com.google.firebase.ml.vision.label.FirebaseVisionLabelDetector;
import com.google.firebase.ml.vision.label.FirebaseVisionLabelDetectorOptions;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;
import com.jrodiz.cloudstoragefirebase.MLImageActivity;

import java.util.ArrayList;
import java.util.List;

public enum MLWrapper {
    ML_MODULE;
    private static final String TAG = MLWrapper.class.getSimpleName();


    public enum ML_OP {
        TEXT_RECOGNITION,
        CODE_RECOGNITION,
        FACE_RECOGNITION,
        LABEL_RECOGNITION;
    }

    public interface IMLListener {
        void onMlOperationSuccess(ML_OP type, String multiline);

        ImageView updateImage();

        void onError(ML_OP type, Throwable e);

    }

    public static MLWrapper getInstance() {
        return ML_MODULE;
    }

    public void requestTextRecognition(@NonNull final Bitmap bitmap, @NonNull final IMLListener listener) {
        FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);
        FirebaseVisionTextRecognizer textRecognizer = FirebaseVision.getInstance().getOnDeviceTextRecognizer();
        textRecognizer.processImage(image)
                .addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
                    @Override
                    public void onSuccess(FirebaseVisionText firebaseVisionText) {
                        List<FirebaseVisionText.TextBlock> blocks = firebaseVisionText.getTextBlocks();
                        StringBuilder result = new StringBuilder();
                        for (FirebaseVisionText.TextBlock block : blocks) {
                            String blockText = block.getText();
                            result.append(blockText);
                            result.append("\n");
                        }

                        listener.onMlOperationSuccess(ML_OP.TEXT_RECOGNITION,
                                result.toString().isEmpty() ? "-" : result.toString());

                    }
                })
                .addOnFailureListener(e -> listener.onError(ML_OP.TEXT_RECOGNITION, e));

    }

    public void requestBarCodeRecognition(@NonNull final Bitmap bitmap, @NonNull final IMLListener listener) {
        FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);
        FirebaseVisionBarcodeDetectorOptions options =
                new FirebaseVisionBarcodeDetectorOptions.Builder()
                        .setBarcodeFormats(
                                FirebaseVisionBarcode.FORMAT_ALL_FORMATS,
                                FirebaseVisionBarcode.FORMAT_CODE_128,
                                FirebaseVisionBarcode.FORMAT_CODE_39,
                                FirebaseVisionBarcode.FORMAT_CODE_93,
                                FirebaseVisionBarcode.FORMAT_CODABAR,
                                FirebaseVisionBarcode.FORMAT_DATA_MATRIX,
                                FirebaseVisionBarcode.FORMAT_EAN_13,
                                FirebaseVisionBarcode.FORMAT_EAN_8,
                                FirebaseVisionBarcode.FORMAT_ITF,
                                FirebaseVisionBarcode.FORMAT_QR_CODE,
                                FirebaseVisionBarcode.FORMAT_UPC_A,
                                FirebaseVisionBarcode.FORMAT_UPC_E,
                                FirebaseVisionBarcode.FORMAT_PDF417,
                                FirebaseVisionBarcode.FORMAT_AZTEC,
                                FirebaseVisionBarcode.FORMAT_QR_CODE,
                                FirebaseVisionBarcode.FORMAT_AZTEC)
                        .build();
        FirebaseVisionBarcodeDetector barcodeDetector = FirebaseVision.getInstance()
                .getVisionBarcodeDetector(options);
        barcodeDetector.detectInImage(image).addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionBarcode>>() {
            @Override
            public void onSuccess(List<FirebaseVisionBarcode> firebaseVisionBarcodes) {
                for (FirebaseVisionBarcode barcode : firebaseVisionBarcodes) {

                    String rawValue = barcode.getRawValue();

                    int valueType = barcode.getValueType();
                    // See API reference for complete list of supported types
                    switch (valueType) {
                        case FirebaseVisionBarcode.TYPE_UNKNOWN:
                        case FirebaseVisionBarcode.TYPE_CONTACT_INFO:
                        case FirebaseVisionBarcode.TYPE_EMAIL:
                        case FirebaseVisionBarcode.TYPE_ISBN:
                        case FirebaseVisionBarcode.TYPE_PHONE:
                        case FirebaseVisionBarcode.TYPE_PRODUCT:
                        case FirebaseVisionBarcode.TYPE_SMS:
                        case FirebaseVisionBarcode.TYPE_TEXT:
                        case FirebaseVisionBarcode.TYPE_GEO:
                        case FirebaseVisionBarcode.TYPE_CALENDAR_EVENT:
                        case FirebaseVisionBarcode.TYPE_DRIVER_LICENSE:
                            Log.d(TAG, "type:" + valueType);
                            break;
                        case FirebaseVisionBarcode.TYPE_WIFI:
                            String ssid = barcode.getWifi().getSsid();
                            String password = barcode.getWifi().getPassword();
                            int type = barcode.getWifi().getEncryptionType();
                            break;
                        case FirebaseVisionBarcode.TYPE_URL:
                            String title = barcode.getUrl().getTitle();
                            String url = barcode.getUrl().getUrl();
                            break;
                    }

                    listener.onMlOperationSuccess(ML_OP.CODE_RECOGNITION, rawValue);
                }
            }
        })
                .addOnFailureListener(e -> listener.onError(ML_OP.CODE_RECOGNITION, e))
                .addOnCompleteListener(task -> {
                    if (task.getResult() == null || task.getResult().isEmpty()) {
                        listener.onError(ML_OP.CODE_RECOGNITION, new IllegalStateException("Not able to recognize some code"));
                    }
                });

    }


    public void requestFaceRecognition(@NonNull final Bitmap bitmap, @NonNull final IMLListener listener) {
        FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);
        FirebaseVisionFaceDetector faceDetector = FirebaseVision.getInstance()
                .getVisionFaceDetector();
        faceDetector.detectInImage(image)
                .addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionFace>>() {
                    @Override
                    public void onSuccess(List<FirebaseVisionFace> firebaseVisionFaces) {
                        List<Rect> faceAreas = new ArrayList<>();
                        for (FirebaseVisionFace face : firebaseVisionFaces) {
                            Rect bounds = face.getBoundingBox();
                            faceAreas.add(bounds);
                            float rotY = face.getHeadEulerAngleY();  // Head is rotated to the right rotY degrees
                            float rotZ = face.getHeadEulerAngleZ();  // Head is tilted sideways rotZ degrees

                            // If landmark detection was enabled (mouth, ears, eyes, cheeks, and
                            // nose available):
                            FirebaseVisionFaceLandmark leftEar = face.getLandmark(FirebaseVisionFaceLandmark.LEFT_EAR);
                            if (leftEar != null) {
                                FirebaseVisionPoint leftEarPos = leftEar.getPosition();
                            }

                            // If classification was enabled:
                            if (face.getSmilingProbability() != FirebaseVisionFace.UNCOMPUTED_PROBABILITY) {
                                float smileProb = face.getSmilingProbability();
                            }
                            if (face.getRightEyeOpenProbability() != FirebaseVisionFace.UNCOMPUTED_PROBABILITY) {
                                float rightEyeOpenProb = face.getRightEyeOpenProbability();
                            }
                            if (face.getLeftEyeOpenProbability() != FirebaseVisionFace.UNCOMPUTED_PROBABILITY) {
                                float leftEyeOpenProb = face.getLeftEyeOpenProbability();
                            }

                            // If face tracking was enabled:
                            if (face.getTrackingId() != FirebaseVisionFace.INVALID_ID) {
                                int id = face.getTrackingId();
                            }
                        }

                        listener.onMlOperationSuccess(ML_OP.FACE_RECOGNITION, "Faces: " + faceAreas.size());

                        if (!faceAreas.isEmpty()
                                && listener.updateImage() != null
                                && listener.updateImage().getDrawable() instanceof BitmapDrawable) {
                            listener.updateImage().setImageBitmap(
                                    drawFaces(faceAreas, ((BitmapDrawable) listener.updateImage().getDrawable()).getBitmap())
                            );
                        }
                    }
                })
                .addOnFailureListener(e -> listener.onError(ML_OP.FACE_RECOGNITION, e));
    }

    private Bitmap drawFaces(List<Rect> faceAreas, Bitmap source) {
        for (Rect rectFace : faceAreas) {
            Canvas canvas = new Canvas(source);
            Paint paint = new Paint();
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(12);
            paint.setColor(Color.RED);
            paint.setAntiAlias(true);

            canvas.drawRect(rectFace, paint);
        }

        return source;
    }


    public void requestLabelRecognition(@NonNull final Bitmap bitmap, @NonNull final IMLListener listener) {
        FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);
        FirebaseVisionLabelDetectorOptions options =
                new FirebaseVisionLabelDetectorOptions.Builder()
                        .setConfidenceThreshold(0.8f)
                        .build();

        FirebaseVisionLabelDetector detector = FirebaseVision.getInstance()
                .getVisionLabelDetector(options);
        detector.detectInImage(image).addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionLabel>>() {
            @Override
            public void onSuccess(List<FirebaseVisionLabel> firebaseVisionLabels) {
                StringBuilder result = new StringBuilder();
                for (FirebaseVisionLabel label : firebaseVisionLabels) {
                    String text = label.getLabel();
                    String entityId = label.getEntityId();
                    float confidence = label.getConfidence();
                    result.append(text)
                            .append(" :: ")
                            .append(confidence)
                            .append("\n");
                }
                listener.onMlOperationSuccess(ML_OP.LABEL_RECOGNITION,
                        result.toString().isEmpty() ? "-" : result.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                listener.onError(ML_OP.LABEL_RECOGNITION, e);
            }
        });
    }
}
