package com.jrodiz.cloudstoragefirebase.firebase.storage;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageException;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StreamDownloadTask;
import com.google.firebase.storage.UploadTask;
import com.jrodiz.cloudstoragefirebase.base.AppConstants;
import com.jrodiz.cloudstoragefirebase.base.IRptContext;
import com.jrodiz.cloudstoragefirebase.db.AppRemoteDb;
import com.jrodiz.cloudstoragefirebase.obj.JImage;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

import static java.util.Arrays.asList;

public enum StorageWrapper {
    STORAGE_MODULE;
    private static final String TAG = StorageWrapper.class.getSimpleName();

    private static final List<Character> NO_VALID_CHARACTERS = asList('#', '[', ']', '*', '?', '$');
    private static final StorageMetadata IMG_METADATA = new StorageMetadata.Builder()
            .setContentType("image/jpg")
            .build();

    private static FirebaseStorage mStorage;

    public interface IDownloadListener extends IRptContext<Context> {

        void onDownloaded(Uri imageUri);

        void onError(int errorCode, Throwable exception);

        abstract class Stub implements IDownloadListener {

            private static final String TAG = IDownloadListener.class.getSimpleName();

            private final JImage mImage;

            protected abstract void onImageUpdated(JImage image);

            public Stub(JImage image) {
                this.mImage = image;
            }

            @Override
            public void onDownloaded(Uri imageUri) {
                Log.d(TAG, mImage + "<< onDownloaded: " + imageUri);
                mImage.setProgress(100);
                mImage.addPath(imageUri);
                onImageUpdated(mImage);
            }

            @Override
            public void onError(int errorCode, Throwable exception) {
                Log.d(TAG, mImage + "<< onError: " + errorCode);
                exception.printStackTrace();
            }
        }
    }

    public interface IUploadListener extends IRptContext<Context> {

        void onStarted(String storagePath);

        String getStoragePath();

        void onProgress(double percent);

        void onUploaded(String fileUri);

        void onError(int errorCode, Throwable exception);

        void onPaused(String filePath, double progress);


        abstract class Stub implements IUploadListener {

            private static final String TAG = IUploadListener.class.getSimpleName();

            private final JImage mImage;

            protected abstract void onImageUpdated(JImage image);

            public Stub(JImage image) {
                this.mImage = image;
            }

            @Override
            public void onStarted(String storagePath) {
                Log.d(TAG, mImage + "<< onStarted: " + storagePath);
                mImage.setStoragePath(storagePath);
                mImage.setProgress(0);
                onImageUpdated(mImage);
            }

            @Override
            public String getStoragePath() {
                Log.d(TAG, mImage + "<< getStoragePath: " + mImage.getStoragePath());
                return mImage.getStoragePath();
            }

            @Override
            public void onProgress(double percent) {
                Log.d(TAG, mImage + "<< onProgress: " + percent);
                mImage.setProgress(percent);
                onImageUpdated(mImage);
            }

            @Override
            public void onUploaded(String fileUri) {
                Log.d(TAG, mImage + "<< onUploaded: " + fileUri);
                mImage.setRemotePath(fileUri);
                onImageUpdated(mImage);

                AppRemoteDb.getInstance().saveImageReference(getImage());
            }

            @Override
            public void onError(int errorCode, Throwable exception) {
                Log.d(TAG, mImage + "<< onError: " + errorCode);
                exception.printStackTrace();
            }

            @Override
            public void onPaused(String filePath, double progress) {
                Log.d(TAG, mImage + "<< onPaused: " + filePath + "::" + progress);

            }

            public JImage getImage() {
                return mImage;
            }
        }
    }

    public static StorageWrapper getInstance() {
        return STORAGE_MODULE.init();
    }

    private StorageWrapper init() {
        mStorage = FirebaseStorage.getInstance();
        return this;
    }

    public void uploadImage(@NonNull final JImage image, @NonNull final IUploadListener listener) {
        uploadImage(null, image, listener);
    }

    public void uploadImage(@Nullable final Executor executor, @NonNull final JImage image, @NonNull final IUploadListener listener) {
        validateImageOrThrow(image);

        final StorageReference imageRef = mStorage.getReference()
                .child(AppConstants.Db.PATH_IMG.concat(image.getName()));

        listener.onStarted(imageRef.toString());

        final UploadTask uploadTask = imageRef.putFile(image.getPath(), IMG_METADATA);
        registerListenerTo(executor, imageRef, uploadTask, listener);
    }


    public void downloadImage(@NonNull final JImage image, @NonNull final IDownloadListener listener) {
        downloadImage(null, image, listener);
    }

    public void downloadImage(@Nullable final Executor executor, @NonNull final JImage emptyImg, @NonNull final IDownloadListener listener) {
        final StorageReference imageRef = mStorage.getReference()
                .child(emptyImg.getRemotePath());

        imageRef.getDownloadUrl()
                .addOnSuccessListener(executor, listener::onDownloaded)
                .addOnFailureListener(executor, e -> listener.onError(AppConstants.INVALID_INT, e));
        Log.d(TAG, "<< downloadingImage: " + imageRef.getBucket());
    }

    @Deprecated
    public void reactivateListener(@NonNull final IUploadListener listener) {
        StorageReference storageRef = mStorage.getReference(listener.getStoragePath());
        List<UploadTask> tasks = storageRef.getActiveUploadTasks();
        for (UploadTask t : tasks) {
            registerListenerTo(null, storageRef, t, listener);
        }
    }

    private void registerListenerTo(@Nullable final Executor executor, StorageReference reference, UploadTask uploadTask, IUploadListener listener) {
        uploadTask.
                addOnProgressListener(executor, taskSnapshot -> {
                    double percent = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                    if (percent > 0) {
                        listener.onProgress(percent);
                    }
                })
                .addOnFailureListener(executor, exception -> listener.onError(((StorageException) exception).getErrorCode(), exception))
                .addOnSuccessListener(executor, taskSnapshot -> listener.onUploaded(taskSnapshot.getMetadata().getPath()))
                .addOnPausedListener(executor, taskSnapshot -> listener.onPaused(taskSnapshot.getMetadata().getPath(), (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount()));

    }

    public static void validateImageOrThrow(JImage image) {
        if (image == null || image.getName() == null || image.getPath() == null) {
            throw new IllegalArgumentException("Some properties are null into " + image.toString());
        }
        if (image.getName().length() < 1 || image.getName().length() > 1024) {
            throw new IllegalArgumentException("Name is not valid " + image.toString());
        }
        for (Character c : image.getName().toCharArray()) {
            if (NO_VALID_CHARACTERS.contains(c)) {
                throw new IllegalArgumentException("Some properties are invalid into name: " + image.toString());
            }
        }
    }
}
