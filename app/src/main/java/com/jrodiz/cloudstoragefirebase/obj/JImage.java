package com.jrodiz.cloudstoragefirebase.obj;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import java.io.File;
import java.util.Objects;

@Entity(tableName = "JImage")
public class JImage implements Parcelable {
    @PrimaryKey(autoGenerate = true)
    private long mId;

    @ColumnInfo(name = "name")
    private String mName;

    @ColumnInfo(name = "uri")
    private Uri mPath;

    @ColumnInfo(name = "progress")
    private double mProgress;

    @ColumnInfo(name = "storage_path")
    private String mStoragePath;

    @ColumnInfo(name = "remote_path")
    private String mRemotePath;

    @ColumnInfo(name = "from_server")
    private boolean mIsIncoming;

    public JImage() {
    }

    protected JImage(Parcel in) {
        mId = in.readLong();
        mName = in.readString();
        mPath = in.readParcelable(Uri.class.getClassLoader());
        mProgress = in.readDouble();
        mStoragePath = in.readString();
        mRemotePath = in.readString();
        mIsIncoming = in.readByte() != 0;
    }

    public static final Creator<JImage> CREATOR = new Creator<JImage>() {
        @Override
        public JImage createFromParcel(Parcel in) {
            return new JImage(in);
        }

        @Override
        public JImage[] newArray(int size) {
            return new JImage[size];
        }
    };

    public String getName() {
        return mName;
    }

    public JImage addName(String name) {
        setName(name);
        return this;
    }

    public void setName(String name) {
        mName = name;
    }


    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public Uri getPath() {
        return mPath;
    }

    public JImage addPath(Uri path) {
        setPath(path);
        return this;
    }

    public JImage setPath(File path) {
        return addPath(Uri.fromFile(path));
    }

    public void setPath(Uri path) {
        mPath = path;
    }

    public double getProgress() {
        return mProgress;
    }

    public void setProgress(double progress) {
        mProgress = progress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JImage image = (JImage) o;

        if (mName != null ? !mName.equals(image.mName) : image.mName != null) return false;
        return mPath != null ? mPath.equals(image.mPath) : image.mPath == null;
    }

    @Override
    public int hashCode() {
        int result = mName != null ? mName.hashCode() : 0;
        result = 31 * result + (mPath != null ? mPath.hashCode() : 0);
        return result;
    }

    public String getStoragePath() {
        return mStoragePath;
    }

    public void setStoragePath(String storagePath) {
        mStoragePath = storagePath;
    }

    public String getRemotePath() {
        return mRemotePath;
    }

    public void setRemotePath(String remotePath) {
        mRemotePath = remotePath;
    }

    public boolean isUploaded() {
        return !TextUtils.isEmpty(mRemotePath);
    }

    public boolean isIncoming() {
        return mIsIncoming;
    }

    public void setIsIncoming(boolean incoming) {
        mIsIncoming = incoming;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mId);
        dest.writeString(mName);
        dest.writeParcelable(mPath, flags);
        dest.writeDouble(mProgress);
        dest.writeString(mStoragePath);
        dest.writeString(mRemotePath);
        dest.writeByte((byte) (mIsIncoming ? 1 : 0));
    }
}
