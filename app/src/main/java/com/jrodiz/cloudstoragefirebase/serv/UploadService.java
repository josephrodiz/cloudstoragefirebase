package com.jrodiz.cloudstoragefirebase.serv;


import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.jrodiz.cloudstoragefirebase.MainActivity;
import com.jrodiz.cloudstoragefirebase.R;
import com.jrodiz.cloudstoragefirebase.base.AppConstants;
import com.jrodiz.cloudstoragefirebase.firebase.storage.StorageWrapper;
import com.jrodiz.cloudstoragefirebase.obj.JImage;

import java.util.List;


public class UploadService extends BaseTaskService {

    private static final String TAG = UploadService.class.getSimpleName();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mExecutorService.submit(new SyncUploadImagesJob());
        return START_NOT_STICKY;
    }

    private class SyncUploadImagesJob implements Runnable {
        @Override
        public void run() {
            List<JImage> mImageLv = mDbController.getImageDao(mServiceContext).getAll();

            for (JImage i : mImageLv) {
                if (!TextUtils.isEmpty(i.getRemotePath()))
                    continue; //Already sync

                StorageWrapper.getInstance().uploadImage(mExecutorService, i, new StorageWrapper.IUploadListener.Stub(i) {
                    @Override
                    public void onStarted(String storagePath) {
                        super.onStarted(storagePath);

                        taskStarted();
                        showProgressNotification(getImage().getId(), true, 0);
                    }

                    @Override
                    public void onUploaded(String fileUri) {
                        super.onUploaded(fileUri);

                        //showUploadFinishedNotification(getImage().getId(), true);
                        dismissProgressNotification(getImage().getId());
                        taskCompleted();
                    }

                    @Override
                    public void onError(int errorCode, Throwable exception) {
                        super.onError(errorCode, exception);

                        //showUploadFinishedNotification(getImage().getId(), false);
                        dismissProgressNotification(getImage().getId());
                        taskCompleted();
                    }

                    @Override
                    public void onProgress(double percent) {
                        super.onProgress(percent);
                        showProgressNotification(getImage().getId(), true, percent);
                    }

                    @Override
                    protected void onImageUpdated(JImage image) {
                        mDbController.getImageDao(getViewContext()).updateAll(image);
                    }

                    @Override
                    public boolean isAlive() {
                        return mServiceContext != null;
                    }

                    @Override
                    public Context getViewContext() {
                        return mServiceContext;
                    }
                });
            }
        }
    }

    /**
     * Show a notification for a finished upload.
     */
    private void showUploadFinishedNotification(final long itemId, final boolean success) {
        // Hide the progress notification
        dismissProgressNotification(itemId);

        Intent intent = new Intent(this, MainActivity.class)
                .putExtra(AppConstants.Extras.ITEM_KEY, itemId)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        String caption = success ? getString(R.string.upload_success) : getString(R.string.upload_failure);
        showFinishedNotification(caption, intent, success);
    }
}
