package com.jrodiz.cloudstoragefirebase.serv;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.annimon.stream.Stream;
import com.jrodiz.cloudstoragefirebase.R;
import com.jrodiz.cloudstoragefirebase.base.AppConstants;
import com.jrodiz.cloudstoragefirebase.db.AppRemoteDb;
import com.jrodiz.cloudstoragefirebase.firebase.storage.StorageWrapper;
import com.jrodiz.cloudstoragefirebase.obj.JImage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DownloadService extends BaseTaskService {

    private static final String TAG = DownloadService.class.getSimpleName();


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand:" + intent + ":" + startId);

        AppRemoteDb.getInstance().requestImagesReferences(
                remoteDbRefs -> {
                    mExecutorService.submit(new SyncDownloadStorage(remoteDbRefs));
                });


        return START_STICKY;
    }

    public class SyncDownloadStorage implements Runnable {

        private final Map<String, String> mRemoteDbRefs;

        private SyncDownloadStorage(Map<String, String> remoteDbRefs) {
            mRemoteDbRefs = new HashMap<>(remoteDbRefs);
        }

        @Override
        public void run() {

            final List<JImage> localImages = mDbController.getImageDao(mServiceContext).getAll();

            boolean pendingToSync = false;
            for (String imageName : mRemoteDbRefs.keySet()) {
                List<JImage> locals = Stream.of(localImages)
                        .filter(it -> it.getName().equalsIgnoreCase(imageName.concat(AppConstants.EXT_JPG))
                                && it.isIncoming())
                        .toList();
                if (locals != null && !locals.isEmpty()) {
                    continue; // Image already sync
                }

                JImage image = new JImage();
                image.setName(imageName.concat(AppConstants.EXT_JPG));
                image.setRemotePath(mRemoteDbRefs.get(imageName));
                image.setIsIncoming(true);

                pendingToSync = true;
                startDownoad(image);
            }

            if (!pendingToSync) {
                taskCompleted(); // Already sync, killing service
            }
        }

        private void startDownoad(final JImage image) {

            taskStarted();
            showProgressNotification(1, false, 0);


            StorageWrapper.getInstance().downloadImage(mExecutorService, image, new StorageWrapper.IDownloadListener.Stub(image) {
                @Override
                public void onDownloaded(Uri imageUri) {
                    super.onDownloaded(imageUri);
                    dismissProgressNotification(1);
                    taskCompleted();
                }

                @Override
                public void onError(int errorCode, Throwable exception) {
                    super.onError(errorCode, exception);
                    dismissProgressNotification(1);
                    taskCompleted();
                }

                @Override
                protected void onImageUpdated(JImage image) {
                    mDbController.getImageDao(mServiceContext).insertAll(image);
                }

                @Override
                public boolean isAlive() {
                    return mServiceContext != null;
                }

                @Override
                public Context getViewContext() {
                    return mServiceContext;
                }
            });
        }
    }

//    private void showDownloadFinishedNotification() {
//        // Hide the progress notification
//        dismissProgressNotification();
//
//        // Make Intent to MainActivity
//        Intent intent = new Intent(this, MainActivity.class)
//                .putExtra(EXTRA_DOWNLOAD_PATH, downloadPath)
//                .putExtra(EXTRA_BYTES_DOWNLOADED, bytesDownloaded)
//                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//
//        boolean success = bytesDownloaded != -1;
//        String caption = success ? getString(R.string.download_success) : getString(R.string.download_failure);
//        showFinishedNotification(caption, intent, true);
//    }
}
