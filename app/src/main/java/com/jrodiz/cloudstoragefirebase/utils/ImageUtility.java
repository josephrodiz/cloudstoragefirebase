package com.jrodiz.cloudstoragefirebase.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.jrodiz.cloudstoragefirebase.base.AppConstants;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class ImageUtility {
    private ImageUtility() {
    }

    public static byte[] toBlob(@NonNull final Bitmap b) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        b.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        return baos.toByteArray();
    }

    public static String makeImageName(boolean addExt) {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return "JPEG_".concat(timeStamp).concat("_").concat(addExt ? AppConstants.EXT_JPG : "");
    }

    @Nullable
    public static File createImageFile(@NonNull final Context context) {
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = null;
        try {
            image = File.createTempFile(
                    makeImageName(false),  /* prefix */
                    AppConstants.EXT_JPG,         /* suffix */
                    storageDir      /* directory */
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Save a file: path for use with ACTION_VIEW intents
        return image;
    }

    public static void galleryAddPic(@NonNull final Context context, @NonNull final File file) {
        galleryAddPic(context, Uri.fromFile(file));
    }

    public static void galleryAddPic(@NonNull final Context context, @NonNull final Uri file) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(file);
        context.sendBroadcast(mediaScanIntent);
    }


    public static Bitmap getBitmap(final ImageView imageView, @NonNull final Uri photoUri) {
        return getBitmap(imageView.getWidth(), imageView.getHeight(), new File(photoUri.getPath()));
    }

    public static Bitmap getBitmap(final ImageView imageView, @NonNull final File photoFile) {
        return getBitmap(imageView.getWidth(), imageView.getHeight(), photoFile);
    }

    public static Bitmap getBitmap(final int targetW, final int targetH, @NonNull final File photoFile) {

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(photoFile.getAbsolutePath(), bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(photoFile.getAbsolutePath(), bmOptions);
        return bitmap;
    }

}
