package com.jrodiz.cloudstoragefirebase.utils;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.jrodiz.cloudstoragefirebase.obj.JImage;

public final class ContextUtils {

    private ContextUtils() {
    }


    @Nullable
    public static Parcelable retrieveParceable(@Nullable final Bundle savedInstanceState,
                                               @Nullable final Bundle intentExtras,
                                               @NonNull final String key) {
        if (savedInstanceState != null && savedInstanceState.containsKey(key)) {
            return savedInstanceState.getParcelable(key);
        } else if (intentExtras != null && intentExtras.containsKey(key)) {
            return intentExtras.getParcelable(key);
        }
        return null;
    }
}
