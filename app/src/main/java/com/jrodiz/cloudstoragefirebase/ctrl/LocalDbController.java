package com.jrodiz.cloudstoragefirebase.ctrl;


import android.content.Context;
import android.support.annotation.NonNull;

import com.jrodiz.cloudstoragefirebase.db.AppLocalDb;
import com.jrodiz.cloudstoragefirebase.db.JImageDao;
import com.jrodiz.cloudstoragefirebase.obj.JImage;

public class LocalDbController extends BaseController {


    public JImageDao getImageDao(@NonNull final Context context) {
        return AppLocalDb.getInstance(context).getImageDao();
    }

    public void upsert(@NonNull final Context context, @NonNull final JImage image) {
        getImageDao(context).insertAll(image);
        getImageDao(context).updateAll(image);

    }
}
