package com.jrodiz.cloudstoragefirebase.ctrl;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.jrodiz.cloudstoragefirebase.R;
import com.jrodiz.cloudstoragefirebase.base.IRptContext;
import com.jrodiz.cloudstoragefirebase.db.AppRemoteDb;
import com.jrodiz.cloudstoragefirebase.db.JImageDao;
import com.jrodiz.cloudstoragefirebase.obj.JImage;
import com.jrodiz.cloudstoragefirebase.serv.DownloadService;
import com.jrodiz.cloudstoragefirebase.serv.UploadService;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class JImageController extends BaseController implements Observer<List<JImage>> {

    private static final String TAG = JImageController.class.getSimpleName();
    private final LocalDbController mDbController = new LocalDbController();
    private final JImageInterface mListener;
    private LiveData<List<JImage>> mImageLv;
    private AtomicBoolean needSyncUpload = new AtomicBoolean(false);

    @Override
    public void onChanged(@Nullable List<JImage> jImages) {
        mListener.onImagesUpdated(jImages);
    }

    public void syncUploadStorage() {
        if (!isNetworkAvailable(mListener.getViewContext())) {
            Toast.makeText(mListener.getViewContext(), R.string.no_network, Toast.LENGTH_SHORT).show();
            return;
        }

        if (mImageLv == null) {
            needSyncUpload.set(true);
            requestLocalImages();
            return;
        }
        performStorageAction(STORAGE_OP.PUSH_ITEM);
    }

    public void syncDownloadStorage() {
        if (!isNetworkAvailable(mListener.getViewContext())) {
            Toast.makeText(mListener.getViewContext(), R.string.no_network, Toast.LENGTH_SHORT).show();
            return;
        }

        if (mImageLv == null) {
            requestLocalImages();
        }

        performStorageAction(STORAGE_OP.PULL_ITEM);
    }


    private void performStorageAction(STORAGE_OP action) {
        Intent serviceAction = null;
        Context context = mListener.getViewContext();
        switch (action) {
            case PUSH_ITEM:
                serviceAction = new Intent(context, UploadService.class);
                break;
            case PULL_ITEM:
                serviceAction = new Intent(context, DownloadService.class);
                break;
        }
        context.startService(serviceAction);

    }
    public interface JImageInterface extends IRptContext<Context> {

        void onImagesUpdated(List<JImage> jImages);

        abstract class Stub implements JImageInterface {
            @Override
            public void onImagesUpdated(List<JImage> jImages) {

            }
        }
    }

    public JImageController(JImageInterface listener) {
        this.mListener = listener;
    }

    public void requestLocalImages() {
        new RequestImagesShortTask(this, DB_OP.GET_ALL).execute();
    }

    public void requestInsertImage(JImage image) {
        new RequestImagesShortTask(this, DB_OP.INSERT).execute(image);
    }

    public void requestUpdateImage(JImage image) {
        new RequestImagesShortTask(this, DB_OP.UPDATE).execute(image);
    }

    private static class RequestImagesShortTask extends AsyncTask<JImage, Void, LiveData<List<JImage>>> {

        private WeakReference<JImageController> mController;

        private final DB_OP mDbOperation;

        public RequestImagesShortTask(JImageController controller, DB_OP dbOp) {
            this.mController = new WeakReference<>(controller);
            this.mDbOperation = dbOp;
        }

        @Override
        protected LiveData<List<JImage>> doInBackground(JImage... images) {
            switch (mDbOperation) {
                case INSERT:
                    mController.get().getDbCtrl().insertAll(images[0]);
                    break;
                case UPDATE:
                    mController.get().getDbCtrl().updateAll(images[0]);
                    break;
                case GET_ALL:
                    return mController.get().getDbCtrl().getLiveAll();
            }
            return null;
        }

        @Override
        protected void onPostExecute(LiveData<List<JImage>> listLiveData) {
            switch (mDbOperation) {
                case INSERT:
                    break;
                case UPDATE:
                    break;
                case GET_ALL:
                    mController.get().onRequestedImages(listLiveData);
            }
        }
    }

    private JImageDao getDbCtrl() {
        return mDbController.getImageDao(mListener.getViewContext());
    }

    private void onRequestedImages(LiveData<List<JImage>> liveData) {
        if (!mListener.isAlive() && !(mListener.getViewContext() instanceof LifecycleOwner)) return;

        mImageLv = liveData;

        //Uploading things
        if (needSyncUpload.get() && mImageLv.getValue() != null) {
            performStorageAction(STORAGE_OP.PUSH_ITEM);
            needSyncUpload.set(false);
            return;
        }

        //Go to UI
        LifecycleOwner owener = (LifecycleOwner) mListener.getViewContext();
        mImageLv.observe(owener, this::onChanged);
    }

}
