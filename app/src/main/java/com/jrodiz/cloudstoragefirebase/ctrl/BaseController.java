package com.jrodiz.cloudstoragefirebase.ctrl;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;

public class BaseController {

    public enum DB_OP {
        UNKNOWN,
        INSERT,
        UPDATE,
        GET_ALL
    }

    public enum STORAGE_OP {
        UNKNOWN,
        PUSH_ITEM,
        PULL_ITEM;
    }


    protected boolean isNetworkAvailable(@NonNull final Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


}
