package com.jrodiz.cloudstoragefirebase.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;
import android.support.annotation.NonNull;

import com.jrodiz.cloudstoragefirebase.base.AppConstants;
import com.jrodiz.cloudstoragefirebase.obj.JImage;

@Database(entities = {JImage.class}, version = AppConstants.Db.DB_VERSION, exportSchema = false)
@TypeConverters({UriTypeConverter.class})
public abstract class AppLocalDb extends RoomDatabase {
    private static AppLocalDb LOCAL_DATABASE = null;

    public synchronized static AppLocalDb getInstance(@NonNull final Context appContext) {
        if (LOCAL_DATABASE == null) {
            LOCAL_DATABASE = Room.<AppLocalDb>databaseBuilder(
                    appContext.getApplicationContext(),
                    AppLocalDb.class,
                    AppConstants.Db.DB_NAME
            ).fallbackToDestructiveMigration().build();
        }
        return LOCAL_DATABASE;
    }

    public abstract JImageDao getImageDao();
}
