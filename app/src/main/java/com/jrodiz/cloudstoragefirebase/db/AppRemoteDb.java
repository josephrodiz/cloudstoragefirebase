package com.jrodiz.cloudstoragefirebase.db;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jrodiz.cloudstoragefirebase.base.AppConstants;
import com.jrodiz.cloudstoragefirebase.firebase.storage.StorageWrapper;
import com.jrodiz.cloudstoragefirebase.obj.JImage;

import java.util.Map;
import java.util.concurrent.Executor;

public enum AppRemoteDb {
    REMOTE_DB;
    private static final String TAG = StorageWrapper.class.getSimpleName();

    private DatabaseReference mRemoteImageRef = null;

    public interface IRemoteDbListener {
        void onImageRefs(Map<String, String> remoteDbRefs);
    }

    public static AppRemoteDb getInstance() {
        return REMOTE_DB.init();
    }

    private AppRemoteDb init() {
        mRemoteImageRef = FirebaseDatabase.getInstance().getReference(AppConstants.Db.PATH_IMG);
        mRemoteImageRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Map<String, String> map = (Map<String, String>) dataSnapshot.getValue();
                if (map != null && !map.isEmpty()) {
                    for (String k : map.keySet())
                        Log.d(TAG, String.format("[%s]=[%s]", k, map.get(k)));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
        return this;
    }

    public void saveImageReference(@NonNull final JImage image) {
        if (mRemoteImageRef == null) {
            throw new IllegalArgumentException("Must call int() first");
        }
        StorageWrapper.validateImageOrThrow(image);

        mRemoteImageRef.child(image.getName().replace(AppConstants.EXT_JPG, ""))
                .setValue(image.getRemotePath());
    }

    public void requestImagesReferences(@NonNull final IRemoteDbListener listener) {
        mRemoteImageRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Map<String, String> map = (Map<String, String>) dataSnapshot.getValue();
                if (map != null) {
                    listener.onImageRefs(map);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
