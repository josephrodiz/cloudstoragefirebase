package com.jrodiz.cloudstoragefirebase.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.jrodiz.cloudstoragefirebase.obj.JImage;

import java.util.List;

@Dao
public interface JImageDao {
    @Query("SELECT * FROM JImage")
    LiveData<List<JImage>> getLiveAll();

    @Query("SELECT * FROM JImage")
    List<JImage> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    List<Long> insertAll(JImage... items);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int updateAll(JImage... items);
}
