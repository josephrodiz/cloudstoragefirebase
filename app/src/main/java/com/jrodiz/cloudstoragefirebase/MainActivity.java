package com.jrodiz.cloudstoragefirebase;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.annimon.stream.Stream;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.jrodiz.cloudstoragefirebase.adapter.ImageAdapter;
import com.jrodiz.cloudstoragefirebase.base.AppConstants;
import com.jrodiz.cloudstoragefirebase.ctrl.JImageController;
import com.jrodiz.cloudstoragefirebase.obj.JImage;
import com.jrodiz.cloudstoragefirebase.utils.ImageUtility;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

import static com.annimon.stream.Collectors.toList;


public class MainActivity extends AppCompatActivity
        implements ImageAdapter.ImageAdapterInterface,
        JImageController.JImageInterface {

    private File mCameraPhotoFile;
    private final List<JImage> mImages = new ArrayList<>();
    private RecyclerView.Adapter mAdapter;
    private final JImageController mJImageController = new JImageController(this);
    private MenuItem mSyncOption;
    private MenuItem mDownloadOption;
    private FloatingActionsMenu mFabMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionsMenu mFab = findViewById(R.id.fab);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mAdapter = new ImageAdapter(this);
        recyclerView.setAdapter(mAdapter);

        mJImageController.requestLocalImages();

        mFabMenu = findViewById(R.id.fab);
        findViewById(R.id.fabGal).setOnClickListener(view -> launchGallery());
        findViewById(R.id.fabCam).setOnClickListener(view -> launchCamera());
    }

    private void launchGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, AppConstants.REQUEST_PERMISION);

                return;
            }
        }

//        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
//                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        galleryIntent.setType("image/*");
//        if (galleryIntent.resolveActivity(getPackageManager()) != null) {
//            startActivityForResult(Intent.createChooser(galleryIntent, "Select File"), RESULT_CODE);
//        }
//        startActivityForResult(galleryIntent, AppConstants.REQUEST_GALLERY_PHOTO);
        Intent intent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
            intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
        } else {
            intent = new Intent(Intent.ACTION_GET_CONTENT);
        }
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select)), AppConstants.REQUEST_GALLERY_PHOTO);
    }

    private void launchCamera() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA}, AppConstants.REQUEST_PERMISION);

            return;
        }

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            mCameraPhotoFile = ImageUtility.createImageFile(this);
            if (mCameraPhotoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        AppConstants.PACKAGE.concat(".fileprovider"),
                        mCameraPhotoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, AppConstants.REQUEST_TAKE_PHOTO);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case AppConstants.REQUEST_PERMISION:
                if (permissions.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (permissions[0].equalsIgnoreCase(Manifest.permission.CAMERA)) {
                        launchCamera();
                    } else {
                        launchGallery();
                    }

                }
                break;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        }

        JImage image = new JImage();
        image.setProgress(0);

        switch (requestCode) {
            case AppConstants.REQUEST_GALLERY_PHOTO:
                if (data == null) return;
                image.addName(ImageUtility.makeImageName(true))
                        .setPath(data.getData());

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    final int takeFlags = data.getFlags() & Intent.FLAG_GRANT_READ_URI_PERMISSION;
                    getContentResolver().takePersistableUriPermission(image.getPath(), takeFlags);
                    //TODO: image.setFlagPer
                }

                break;
            case AppConstants.REQUEST_TAKE_PHOTO:
                if (mCameraPhotoFile == null) return;
                image.addName(mCameraPhotoFile.getName())
                        .setPath(mCameraPhotoFile);
                ImageUtility.galleryAddPic(this, image.getPath());

                break;
            default:
                return;
        }

        if (image.getPath() != null) {
            mJImageController.requestInsertImage(image);
            mFabMenu.collapse();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        mSyncOption = menu.findItem(R.id.sync);
        mDownloadOption = menu.findItem(R.id.to_download);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        mDownloadOption.setVisible(true);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.to_download:
                startActivity(new Intent(this, DownloadActivity.class));
                break;
            case R.id.sync:
                mJImageController.syncUploadStorage();
                break;
        }
        return true;
    }

    @Override
    public boolean isAlive() {
        return !isFinishing();
    }

    @Override
    public Context getViewContext() {
        return this;
    }

    @Override
    public List<JImage> getData() {
        return mImages;
    }

    @Override
    public void onImagesUpdated(List<JImage> jImages) {
        mImages.clear();
        mImages.addAll(Stream.of(jImages).filter(it -> !it.isIncoming()).collect(toList()));
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
        boolean needSync = Stream.of(mImages).filter(it -> !it.isUploaded()).count() > 0;
        if (mSyncOption != null) {
            mSyncOption.setVisible(needSync);
        }
    }
}
