package com.jrodiz.cloudstoragefirebase.base;

public interface IRptContext<T> {

    boolean isAlive();

    T getViewContext();
}
