package com.jrodiz.cloudstoragefirebase.base;

import com.jrodiz.cloudstoragefirebase.BuildConfig;

public final class AppConstants {

    public static final String PACKAGE = BuildConfig.APPLICATION_ID;
    public static final int REQUEST_TAKE_PHOTO = 1;
    public static final int REQUEST_PERMISION = 2;
    public static final int REQUEST_GALLERY_PHOTO = 3;
    public static final int INVALID_INT = -1;
    public static final String EXT_JPG = ".jpg";
    public static final String IMAGE_KEY = PACKAGE.concat(".IMAGE_KEY");

    private AppConstants() {
    }

    public static class Db {
        public static final int DB_VERSION = 9;
        public static final String DB_NAME = "database.db";
        public static final String PATH_IMG = "IMAGES/";
    }

    public static class Extras {
        public static final String ITEM_KEY = PACKAGE.concat("ITEM_KEY");
        public static final String OPERATION_KEY = PACKAGE.concat("OPERATION_KEY");
    }
}
