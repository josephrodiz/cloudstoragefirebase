package com.jrodiz.cloudstoragefirebase;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ablanco.zoomy.Zoomy;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.jrodiz.cloudstoragefirebase.base.AppConstants;
import com.jrodiz.cloudstoragefirebase.firebase.ml.MLWrapper;
import com.jrodiz.cloudstoragefirebase.obj.JImage;
import com.jrodiz.cloudstoragefirebase.utils.ContextUtils;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.IOException;

public class MLImageActivity extends AppCompatActivity implements MLWrapper.IMLListener {
    private static final String TAG = MLImageActivity.class.getSimpleName();

    private JImage mJImage;
    private ImageView mImageView;
    private TextView mDataView;
    private Uri mTempCrop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mlimage);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mJImage = (JImage) ContextUtils.retrieveParceable(savedInstanceState, getIntent().getExtras(), AppConstants.IMAGE_KEY);
        mImageView = findViewById(R.id.image);
        mDataView = findViewById(R.id.data);
        setImage(mJImage.getPath());
    }

    @Override
    protected void onStart() {
        super.onStart();

        Zoomy.Builder builder = new Zoomy.Builder(this).target(mImageView);
        builder.register();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.edit:
                goToEditImage();
                break;
        }
        return true;
    }

    private void goToEditImage() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                    AppConstants.REQUEST_PERMISION);
            return;
        }
        try {
            mTempCrop = Uri.fromFile(File.createTempFile("TEMP", AppConstants.EXT_JPG));
            UCrop.Options opts = new UCrop.Options();
            opts.setToolbarColor(getResources().getColor(R.color.colorPrimary));
            opts.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
            opts.setActiveWidgetColor(getResources().getColor(R.color.colorAccent));
            UCrop.of(mJImage.getPath(), mTempCrop)
                    .withOptions(opts)
                    //.withAspectRatio(16, 9)
                    //.withMaxResultSize(maxWidth, maxHeight)
                    .start(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case AppConstants.REQUEST_PERMISION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    goToEditImage();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            final Uri resultUri = UCrop.getOutput(data);
            setImage(resultUri);
        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(AppConstants.IMAGE_KEY, mJImage);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onMlOperationSuccess(MLWrapper.ML_OP type, String multiline) {
        mDataView.setText(mDataView.getText().toString()
                .concat("\n\n")
                .concat(type.name())
                .concat("\n")
                .concat(multiline));
    }

    @Override
    public ImageView updateImage() {
        return mImageView;
    }

    @Override
    public void onError(MLWrapper.ML_OP type, Throwable e) {
        mDataView.setText(mDataView.getText().toString()
                .concat("\n\n")
                .concat(type.name())
                .concat("\n")
                .concat("-"));
        e.printStackTrace();
    }

    @Override
    protected void onStop() {
        Zoomy.unregister(mImageView);

        super.onStop();
    }


    private void setImage(Uri path) {
        Glide.with(this)
                .asBitmap()
                .load(path)
                .into(new BitmapImageViewTarget(mImageView) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        super.setResource(resource);
                        if (resource == null) {
                            return;
                        }
                        mDataView.setText(null);

                        MLWrapper.getInstance().requestTextRecognition(resource, MLImageActivity.this);
                        MLWrapper.getInstance().requestBarCodeRecognition(resource, MLImageActivity.this);
                        MLWrapper.getInstance().requestLabelRecognition(resource, MLImageActivity.this);
                        MLWrapper.getInstance().requestFaceRecognition(resource, MLImageActivity.this);
                        //MLWrapper.getInstance().requestLandmarkRecognition(resource, MLImageActivity.this);
                    }
                });
    }

}
